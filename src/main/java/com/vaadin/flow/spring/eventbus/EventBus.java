package com.vaadin.flow.spring.eventbus;

import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.cop.support.BeanUtil;
import org.springframework.core.MethodIntrospector;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ReflectionUtils;

import javax.annotation.PostConstruct;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EventObject;
import java.util.List;
import java.util.function.Consumer;

@SpringComponent
@UIScope
public class EventBus {

    public static final ReflectionUtils.MethodFilter UI_EVENT_METHODS = method ->
            (AnnotationUtils.findAnnotation(method, UiEvent.class) != null);


    public static void notify(EventObject eventObject) {
        BeanUtil.getBean(EventBus.class).push(eventObject);
    }

    private List<EventBusConsumer> consumers = new ArrayList<>();

    @PostConstruct
    protected void register() {
        Arrays.stream(BeanUtil.getBeanNamesForAnnotation(UiEventBus.class)).forEach(beanName -> {
            MethodIntrospector.selectMethods(BeanUtil.getBeanType(beanName), UI_EVENT_METHODS)
                    .stream().forEach(m -> registerConsumer(beanName, m, (Class<? extends EventObject>) m.getParameters()[0].getType()));
        });
    }

    public void push(EventObject eventObject) {
        consumers.stream()
                .filter(p->eventObject.getClass().isAssignableFrom(p.getEventObjectType()))
                .forEach(p->p.accept(eventObject));
    }

    private void registerConsumer(String beanName, Method event, Class<? extends EventObject> eventObjectType) {
        EventBusConsumer ebc = new EventBusConsumer(beanName, event, eventObjectType);
        consumers.add(ebc);
    }

    class EventBusConsumer implements Consumer<EventObject> {

        private final String beanName;

        private final Method event;

        private final Class<? extends EventObject> eventObjectType;

        EventBusConsumer(String beanName, Method event, Class<? extends EventObject> eventObjectType) {
            this.beanName = beanName;
            this.event = event;
            this.eventObjectType = eventObjectType;
        }

        @Override
        public void accept(EventObject eventObject) {
            try {
                AopUtils.invokeJoinpointUsingReflection(BeanUtil.getBean(beanName), event, new Object[]{eventObject});
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }

        public Class<? extends EventObject> getEventObjectType() {
            return eventObjectType;
        }

    }

}
